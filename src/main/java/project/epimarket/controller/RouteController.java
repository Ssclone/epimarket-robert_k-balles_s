package project.epimarket.controller;

import java.io.IOException;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import project.epimarket.service.EpimarketService;

/**
 * Main controller of the application.
 * @author steven
 *
 */
@Controller
public class RouteController {

	@Inject
	private EpimarketService service;
	
	@RequestMapping(value = { "/" }, method = { RequestMethod.GET })
	public String getHome(ModelMap model)
			throws IOException {
		
		model.addAttribute("products", service.getProducts());
		return "index";
	}

}
