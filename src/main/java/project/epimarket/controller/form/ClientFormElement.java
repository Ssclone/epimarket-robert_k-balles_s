package project.epimarket.controller.form;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
/**
 * Form bean used to create a Client.
 * @author nicolas
 *
 */
public class ClientFormElement {

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@NotEmpty
	String name;

	@NotEmpty
	@Email
	String email;

	@NotEmpty
	String password;
}
