package project.epimarket.controller.form;

import org.hibernate.validator.constraints.NotEmpty;
/**
 * Form bean used by a teacher to change his password.
 * @author nicolas
 *
 */
public class UserChangePasswordForm {

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getConfirmation() {
		return confirmation;
	}

	public void setConfirmation(String confirmation) {
		this.confirmation = confirmation;
	}

	@NotEmpty
	String newPassword;
	@NotEmpty
	String confirmation;
	@NotEmpty
	String oldPassword;

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

}
