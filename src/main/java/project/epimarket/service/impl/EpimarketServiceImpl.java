package project.epimarket.service.impl;

import java.util.Collection;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import project.epimarket.exception.InvalidPasswordException;
import project.epimarket.exception.NoSuchClientException;
import project.epimarket.model.Client;
import project.epimarket.model.Product;
import project.epimarket.repositories.ClientRepository;
import project.epimarket.repositories.ProductRepository;
import project.epimarket.service.EpimarketService;

/**
 * concrete and only implementation of the service.
 * @author Tim
 *
 */
@Service
public class EpimarketServiceImpl implements EpimarketService {

	@Inject
	ProductRepository productRepo;

	@Inject
	public
	ClientRepository clientRepo;

	@Inject
	public
	PasswordEncoder passwordEncoder;

	public Collection<Product> getProducts() {
		return productRepo.findAll();

	}

	public Collection<Client> getClients() {
		return clientRepo.findAll();

	}

	public void deleteProduct(Integer id) throws NoSuchClientException {
		Product product = productRepo.findOne(id);
		if (product == null)
			throw new NoSuchClientException();
		productRepo.delete(product);

	}

	public Client addClient(String name, String mail, String password) {

		Client client = new Client();
		client.setName(name);
		client.setMail(mail);
		client.setPassword(passwordEncoder.encode(password));

		client = clientRepo.save(client);
		clientRepo.flush();
		LOGGER.info("Un nouveau client a été créé: %", name);
		return client;
	}


	private static final Logger LOGGER = LoggerFactory
			.getLogger(EpimarketServiceImpl.class);

	@Transactional
	@Override
	public Client getClient(Integer idClient) {
		Client t = clientRepo.findOne(idClient);

		return t;
	}

	@Override
	public void changePassword(String name, String newPassword,
			String oldPassword) throws  NoSuchClientException, InvalidPasswordException {
		Client client = clientRepo.findByName(name);
		if (client== null) {
			throw new NoSuchClientException();
		}
		
		if (!passwordEncoder.matches(oldPassword, client.getPassword())) {
			throw new InvalidPasswordException();
		}

		client.setPassword(passwordEncoder.encode(newPassword));

		clientRepo.saveAndFlush(client);
	}


	@Override
	public void deleteClient(Integer id) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Product addProduct(String name) {
		// TODO Auto-generated method stub
		return null;
	}

}
