package project.epimarket.service;

import java.util.Collection;

import org.springframework.security.access.prepost.PreAuthorize;

import project.epimarket.exception.InvalidPasswordException;
import project.epimarket.exception.NoSuchClientException;
import project.epimarket.model.Client;
import project.epimarket.model.Product;

/**
 * Main service for teacher/course business.
 * @author Tim
 *
 */
public interface EpimarketService {
	
	public	Collection<Product> getProducts();

		
	public	Collection<Client>	getClients();
	
	// public Collection<Pair<DateTime, DateTime>> getAvaibleTimeSlot(Duration
	// d);

	@PreAuthorize("hasAnyRole('ADMIN')")
	public	Product	addProduct(String name);
	
	@PreAuthorize("hasRole('ADMIN')")
	public void deleteProduct(Integer id) throws NoSuchClientException;
	
	@PreAuthorize("hasRole('ADMIN')")
	public	Client	addClient(String name, String password, String email);
		
	@PreAuthorize("hasRole('ADMIN')")
	public void deleteClient(Integer id) throws Exception;
	
	public Client	getClient(Integer idClient);
	
	@PreAuthorize("isAuthenticated()")
	public void changePassword(String name, String newPassword,
			String oldPassword) throws  NoSuchClientException, InvalidPasswordException;
}