package project.epimarket.repositories.custom;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.springframework.stereotype.Component;

import project.epimarket.model.Client;
import project.epimarket.model.QClient;

import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;

@Component
public class ClientRepositoryCustomImpl implements ClientRepositoryCustom {
	
	@Inject
	EntityManagerFactory emFactory;

	@Override
	public Client getClientTest() {
		EntityManager em = emFactory.createEntityManager();
		try {
			JPQLQuery query = new JPAQuery(em);
			QClient client = QClient.client;
			List<Client> res = query.from(client)
					.where(client.name.eq("Jack")).list(client);
			return res.get(0);
		} finally {
			em.close();
	}
	}
}
