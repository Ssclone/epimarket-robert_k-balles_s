package project.epimarket.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import project.epimarket.model.Order;

public interface OrderRepository extends JpaRepository<Order, Integer>, 
		QueryDslPredicateExecutor<Order> {
	
	public Order findById(int id);
}
