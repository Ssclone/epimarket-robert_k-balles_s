package project.epimarket.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import project.epimarket.model.Product;

public interface ProductRepository extends JpaRepository<Product, Integer>, 
		QueryDslPredicateExecutor<Product> {
	
	public Product findByName(String name);
}
