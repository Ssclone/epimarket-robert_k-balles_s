package project.epimarket.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import project.epimarket.model.Client;

public interface ClientRepository extends JpaRepository<Client, Integer>, 
		QueryDslPredicateExecutor<Client> {
	
	public Client findByName(String name);

	public Client findByMail(String mail);
}
