package project.epimarket.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import project.epimarket.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Integer>, 
		QueryDslPredicateExecutor<Category> {
	
	public Category findByName(String name);
}
