package project.epimarket.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * This class replaces web.xml content.
 * 
 * @author nicolas
 */
public class MessageWebApplicationInitializer extends
		AbstractAnnotationConfigDispatcherServletInitializer {

	/**
	 * plug the mvc configuration info the web.xml initalization.
	 */
	protected final Class<?>[] getRootConfigClasses() {
		return new Class<?>[] { MvcConfiguration.class };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class<?>[] { MvcConfiguration.class };
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

}
