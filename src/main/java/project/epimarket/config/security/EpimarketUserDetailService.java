package project.epimarket.config.security;

import java.util.Arrays;
import java.util.Collection;

import javax.inject.Inject;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import project.epimarket.model.Access;
import project.epimarket.model.Client;
import project.epimarket.repositories.ClientRepository;
import project.epimarket.repositories.custom.ClientRepositoryCustom;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

/**
 * this class use data access to provide a way to authenticate users
 * @author steven
 *
 */
@Service
public class EpimarketUserDetailService implements UserDetailsService {

	@Inject
	ClientRepository ClientRepo;

	@Inject
	PasswordEncoder passwordEncoder;
	
	@Inject
	ClientRepositoryCustom custom;


	@Transactional
	// @Transactional annotation is used to access lazy initialization for
	// domain object
	@Override
	public UserDetails loadUserByUsername(String mail)
			throws UsernameNotFoundException {

		// master authentication (hard coded for convenience)
		if ("test".equals(mail)) {
			SimpleGrantedAuthority authority = new SimpleGrantedAuthority(
					"ADMIN");
			User user = new User("test",
					passwordEncoder.encode("test"), Arrays.asList(authority));
			return user;

		}
		
		// find a Client by mail
		for(Client client : ClientRepo.findAll()){
			System.out.println(client.getName());
		}
		Client Clients = ClientRepo.findByMail(mail);
		if (Clients == null) {
			// bad credentials
			throw new UsernameNotFoundException(mail);
		}
		// from the Access table, we get roles as strings and convert them as
		// Granted Authority, using Guava
		Collection<GrantedAuthority> accessAsGrantedAuthority = Lists
				.newArrayList(Iterables.transform(Clients.getAccesses(),
						new Function<Access, GrantedAuthority>() {

							@Override
							public GrantedAuthority apply(Access access) {
								SimpleGrantedAuthority authority = new SimpleGrantedAuthority(
										access.getName());
								return authority;

							}

						}));

		// plug user info and well as authorities
		User user = new User(Clients.getMail(), Clients.getPassword(),
				accessAsGrantedAuthority);
		
		
		return user;
	}
}
