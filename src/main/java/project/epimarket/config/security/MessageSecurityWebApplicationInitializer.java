package project.epimarket.config.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * initialize security.	
 * @author nicolas
 *
 */
public class MessageSecurityWebApplicationInitializer extends
		AbstractSecurityWebApplicationInitializer {
}
