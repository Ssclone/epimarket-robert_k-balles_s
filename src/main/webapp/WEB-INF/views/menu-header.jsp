<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<c:url value="/" var="webappRoot">
</c:url>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="${webappRoot}/resources/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link href="${webappRoot}/resources/bootstrap-theme.min.css"
	rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>

	<h1>Epimarket</h1>

	<ul>
		<sec:authorize access="isAuthenticated()">
			<li><a href="${webappRoot}caddy">My caddy</a></li>
			<li><a href="${webappRoot}secure/my-account">my account</a></li>
			<li><a href="${webappRoot}logout">logout</a></li>
		</sec:authorize>
		<sec:authorize ifAllGranted="ADMIN">
			<li>Admin Features
				<ul>
					<li><a href="${webappRoot}new-product">add a new product</a></li>
					<li><a href="${webappRoot}delete-product">delete a product</a></li>
					<li><a href="${webappRoot}show-order">show orders</a></li>					
				</ul>
			</li>
		</sec:authorize>
	</ul>


	