package project.epimarket.service.impl;

import static org.junit.Assert.fail;

import org.easymock.EasyMock;
import org.junit.Test;
import org.springframework.security.crypto.password.PasswordEncoder;

import project.epimarket.model.Client;
import project.epimarket.exception.InvalidPasswordException;
import project.epimarket.exception.NoSuchClientException;
import project.epimarket.repositories.ClientRepository;
import project.epimarket.service.impl.EpimarketServiceImpl;

public class EpimarketServiceTest {

	@Test
	public void test() {
		EpimarketServiceImpl service = new EpimarketServiceImpl();

		// create objects to test
		Client nicolas = new Client();
		nicolas.setName("nicolas");
		nicolas.setMail("nicolas@mirlitone.com");
		nicolas.setPassword("toto");

		// configurer les repository avec easymock
		ClientRepository repo = EasyMock.createNiceMock(ClientRepository.class);
		EasyMock.expect(repo.findByMail(EasyMock.eq(nicolas.getMail())))
				.andReturn(nicolas).anyTimes();
		EasyMock.expect(repo.saveAndFlush(EasyMock.anyObject(Client.class)))
				.andReturn(null).anyTimes();
		EasyMock.replay(repo);

		// configure password encoder
		PasswordEncoder encoder = EasyMock
				.createNiceMock(PasswordEncoder.class);
		EasyMock.expect(
				encoder.matches(EasyMock.eq("toto"), EasyMock.eq("toto")))
				.andReturn(Boolean.TRUE).anyTimes();
		EasyMock.expect(
				encoder.matches(EasyMock.anyString(), EasyMock.anyString()))
				.andReturn(Boolean.FALSE).anyTimes();

		EasyMock.expect(encoder.encode(EasyMock.eq("toto"))).andReturn("toto")
				.anyTimes();
		EasyMock.replay(encoder);

		service.passwordEncoder = encoder;
		service.clientRepo = repo;

		try {
			service.changePassword("nicolas@mirlitone.com", "titi", "toto");
		} catch (NoSuchClientException | InvalidPasswordException e) {
			fail("should not have thrown");
		}

		try {
			service.changePassword("nicolas@mirlitone.com", "tata", "titi");
			fail("should have thrown");
		} catch (NoSuchClientException e) {
			fail("should not have thrown");
		} catch (InvalidPasswordException e) {

		}

		try {
			service.changePassword("nicolas2@mirlitone.com", "tata", "titi");
			fail("should not have thrown");
		} catch (NoSuchClientException e) {

		} catch (InvalidPasswordException e) {
			fail("should not have thrown");
		}

	}

}
