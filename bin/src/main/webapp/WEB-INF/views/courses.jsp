<%@ include file="menu-header.jsp" %>

<table class="table">
	<tr>
		<th>Titre du Cours</th>
		<th>Prof</th>
	</tr>
	
	<c:forEach var="cours" items="${courses}">
		<tr>

			<td><c:out value="${cours.name}"></c:out></td>

			<td><a href="${webappRoot}/teacher/${cours.teacher.id}"><c:out
						value="${cours.teacher.name}" /></a></td>
			<td><c:url value="delete-course" var="deleteCourseUrl">
					<c:param name="courseId" value="${cours.id}"></c:param>
				</c:url> <a href="${deleteCourseUrl}">X</a></td>
		</tr>
	</c:forEach>
</table>

<%@ include file="menu-footer.jsp" %>