<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<c:url value="/" var="webappRoot">
</c:url>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="${webappRoot}/resources/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link href="${webappRoot}/resources/bootstrap-theme.min.css"
	rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>

	<h1>Menu</h1>

	<ul>
		<sec:authorize ifAllGranted="ADMIN">
			<li>Admin Features
				<ul>

					<li><a href="${webappRoot}unemployed-teachers">show
							unemployed teachers</a></li>
					<li><a href="${webappRoot}nouveau-teacher">add a new
							teacher</a></li>

				</ul>
			</li>
		</sec:authorize>

		<sec:authorize access="isAuthenticated()">
			<li><a href="${webappRoot}secure/my-account">my account</a></li>
		</sec:authorize>
		<li><a href="${webappRoot}nouveau-cours">add a new course</a></li>
		<li><a href="${webappRoot}teachers">show all teachers</a></li>
		<li><a href="${webappRoot}logout">logout</a></li>


	</ul>

	<h1>Contenu</h1>