package project.epimarket.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QClient is a Querydsl query type for Client
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QClient extends EntityPathBase<Client> {

    private static final long serialVersionUID = 1137146713L;

    public static final QClient client = new QClient("client");

    public final ListPath<Access, QAccess> accesses = this.<Access, QAccess>createList("accesses", Access.class, QAccess.class, PathInits.DIRECT2);

    public final StringPath address = createString("address");

    public final DatePath<java.util.Date> birthdate = createDate("birthdate", java.util.Date.class);

    public final StringPath city = createString("city");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath mail = createString("mail");

    public final StringPath name = createString("name");

    public final ListPath<Order, QOrder> orders = this.<Order, QOrder>createList("orders", Order.class, QOrder.class, PathInits.DIRECT2);

    public final StringPath password = createString("password");

    public final NumberPath<Integer> zipcode = createNumber("zipcode", Integer.class);

    public QClient(String variable) {
        super(Client.class, forVariable(variable));
    }

    public QClient(Path<? extends Client> path) {
        super(path.getType(), path.getMetadata());
    }

    public QClient(PathMetadata<?> metadata) {
        super(Client.class, metadata);
    }

}

