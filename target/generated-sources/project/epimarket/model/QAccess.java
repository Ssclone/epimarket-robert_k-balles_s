package project.epimarket.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QAccess is a Querydsl query type for Access
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QAccess extends EntityPathBase<Access> {

    private static final long serialVersionUID = 1071398130L;

    public static final QAccess access = new QAccess("access");

    public final ListPath<Client, QClient> clients = this.<Client, QClient>createList("clients", Client.class, QClient.class, PathInits.DIRECT2);

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath name = createString("name");

    public QAccess(String variable) {
        super(Access.class, forVariable(variable));
    }

    public QAccess(Path<? extends Access> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAccess(PathMetadata<?> metadata) {
        super(Access.class, metadata);
    }

}

